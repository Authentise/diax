#!/usr/bin/env fish
echo "Removing dist/*"
rm -f dist/*
echo "Removing __pycache__ directories"
find . | grep -E "(__pycache__)" | xargs rm -rf
echo "Running pylint"
if not ./pylint.fish
	echo "Pylint failed"
	exit 1
end
echo "Running unit tests"
if not py.test --cov-report xml --cov diax --junitxml dist/results.xml tests
	echo "Unit tests failed"
	exit 2
end
echo "Building the python package"
set build_version (python setup.py version)
echo "Found build version '$build_version'"
if test $build_version = "0.0.1"
	echo "Building package without upload"
	python setup.py sdist
else
	echo "Building package and uploading"
	python setup.py sdist upload
end
